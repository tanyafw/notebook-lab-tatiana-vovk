﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotePad
{
    class Note
    {
        static Dictionary<int,Note> allNotes = new Dictionary<int,Note>();
        readonly int id; //req
        public string surname; //req
        public string name; //req
        public string patronymic = "-";
        public string phone; //req
        public string country; //req
        public DateTime birthDate = new DateTime();
        public string organization = "-";
        public string position = "-";
        public string comments = "-";
        public Note(int id1, string surname1, string name1, string phone1, string country1)
        {
            this.id = id1;
            this.surname = surname1;
            this.name = name1;
            this.phone = phone1;
            this.country = country1;
        }
        public static void NewNote(int cnt)
        {
            Console.WriteLine("You have selected to create a note. Please, provide the information");
            string sur = ""; 
            while (true)
            {
                Console.Write("SURNAME (required): ");
                sur = Console.ReadLine();
                if (sur != "")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Please, be attentive, SURNAME is required! Try again");
                }
            }
            string nam = "";
            while (true)
            {
                Console.Write("NAME (required): ");
                nam = Console.ReadLine();
                if (nam != "")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Please, be attentive, NAME is required! Try again");
                }
            }
            Console.Write("PATRONYMIC (to skip press enter): ");
            string patron = Console.ReadLine();
            string ph = "";
            while (true)
            {
                Console.Write("PHONE (digits only, required): ");
                ph = Console.ReadLine();
                if (ph == "")
                {
                    Console.WriteLine("Please, be attentive, PHONE is required! Try again");
                }
                else if (ph.All(char.IsDigit))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Please, be careful, PHONE must contain only digits from 0 to 9!");
                }
            }
            string count = "";
            while (true)
            {
                Console.Write("COUNTRY (required): ");
                count = Console.ReadLine();
                if (count != "")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Please, be attentive, COUNTRY is required! Try again");
                }
            }
            int d, m, y;
            string bdate = "";
            while (true)
            {
                Console.Write("BIRTHDATE (DD.MM.YYYY, to skip press enter): ");
                bdate = Console.ReadLine();
                char[] bdateCharArray = bdate.ToCharArray();
                bool crit = true;
                if (bdate != "")
                {
                    for (int i = 0; i < bdate.Split('.').Length; i++)
                    {
                        crit &= bdate.Split('.')[i].All(char.IsNumber);
                    }
                    crit &= bdateCharArray[2] == '.' && bdateCharArray[5] == '.' && bdateCharArray.Length == 10;
                }
                if (crit)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Please, be careful, BIRTHDATE must have the form DD.MM.YYYY!");
                }
            }
            Console.Write("ORGANIZATION (to skip press enter): ");
            string org = Console.ReadLine();
            Console.Write("POSITION (to skip press enter): ");
            string pos = Console.ReadLine();
            Console.Write("COMMENTS (to skip press enter): ");
            string com = Console.ReadLine();


            Note note = new Note(cnt, sur, nam, ph, count);
            allNotes.Add(cnt, note);
            if (patron != "")
            {
                note.patronymic = patron;
            }
            if (bdate != "")
            {
                d = int.Parse(bdate.Split('.')[0]);
                m = int.Parse(bdate.Split('.')[1]);
                y = int.Parse(bdate.Split('.')[2]);
                note.birthDate = new DateTime(y,m,d);
            }
            if (org != "")
            {
                note.organization = org;
            }
            if (pos != "")
            {
                note.position = pos;
            }
            if (com != "")
            {
                note.comments = com;
            }

            Console.WriteLine("A new note has been created!");
        }
        public static void UpdateNote(int idUpd)
        {
            bool nonex = true;
            foreach (KeyValuePair<int, Note> kv in allNotes)
            {
                if (kv.Key == idUpd)
                {
                    while (true)
                    {
                        Console.WriteLine("What do you want to update in the note with ID " + idUpd + "? Please, select");
                        Console.WriteLine("SURNAME - enter 1");
                        Console.WriteLine("NAME - enter 2");
                        Console.WriteLine("PATRONYMIC - enter 3");
                        Console.WriteLine("PHONE - enter 4");
                        Console.WriteLine("COUNTRY - enter 5");
                        Console.WriteLine("BIRTHDATE - enter 6");
                        Console.WriteLine("ORGANIZATION - enter 7");
                        Console.WriteLine("POSITION - enter 8");
                        Console.WriteLine("COMMENTS - enter 9");
                        Console.WriteLine("To end the updating session, please, enter 0");
                        string pp = "";
                        while (true)
                        {
                            pp = Console.ReadLine();
                            if (pp.All(char.IsDigit) && pp != "")
                            {
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Please, be careful and try again, you should enter digits ONLY!");
                            }
                        }
                        int p = int.Parse(pp);
                        if (p == 0)
                        {
                            break;
                        }
                        else if (p == 1)
                        {
                            string sur = "";
                            while (true)
                            {
                                Console.WriteLine("OLD SURNAME: " + kv.Value.surname);
                                Console.Write("NEW SURNAME: ");
                                sur = Console.ReadLine();
                                if (sur == "")
                                {
                                    Console.WriteLine("Please, be attentive, SURNAME is required! Try again");
                                }
                                else
                                {
                                    break;
                                }
                            }
                            kv.Value.surname = sur;
                            Console.WriteLine("SURNAME has been updated!");
                        }
                        else if (p == 2)
                        {
                            string nam = "";
                            while (true)
                            {
                                Console.WriteLine("OLD NAME: " + kv.Value.name);
                                Console.Write("NEW NAME: ");
                                nam = Console.ReadLine();
                                if (nam == "")
                                {
                                    Console.WriteLine("Please, be attentive, NAME is required! Try again");
                                }
                                else
                                {
                                    break;
                                }
                            }
                            kv.Value.name = nam;
                            Console.WriteLine("NAME has been updated!");
                        }
                        else if (p == 3)
                        {
                            Console.WriteLine("OLD PATRONYMIC: " + kv.Value.patronymic);
                            Console.Write("NEW PATRONYMIC: ");
                            kv.Value.patronymic = Console.ReadLine();
                            Console.WriteLine("PATRONYMIC has been updated!");
                        }
                        else if (p == 4)
                        {
                            string ph = "";
                            while (true)
                            {
                                Console.WriteLine("OLD PHONE: " + kv.Value.phone);
                                Console.Write("NEW PHONE (digits only): ");
                                ph = Console.ReadLine();
                                if (ph == "")
                                {
                                    Console.WriteLine("Please, be attentive, PHONE is required! Try again");
                                }
                                else if (ph.All(char.IsDigit))
                                {
                                    kv.Value.phone = ph;
                                    Console.WriteLine("PHONE has been updated!");
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("Please, be careful, PHONE must contain only digits from 0 to 9!");
                                }
                            }
                        }
                        else if (p == 5)
                        {
                            string count = "";
                            while (true)
                            {
                                Console.WriteLine("OLD COUNTRY: " + kv.Value.name);
                                Console.Write("NEW COUNTRY: ");
                                count = Console.ReadLine();
                                if (count == "")
                                {
                                    Console.WriteLine("Please, be attentive, COUNTRY is required! Try again");
                                }
                                else
                                {
                                    break;
                                }
                            }
                            kv.Value.country = count;
                            Console.WriteLine("COUNTRY has been updated!");
                        }
                        else if (p == 6)
                        {
                            string bdate = "";
                            int d, m, y;
                            while (true)
                            {
                                if (kv.Value.birthDate.ToShortDateString() != "01.01.0001")
                                {
                                    Console.WriteLine("OLD BIRTHDATE: " + kv.Value.birthDate.ToShortDateString());
                                }
                                else
                                {
                                    Console.WriteLine("OLD BIRTHDATE: -");
                                }
                                Console.Write("NEW BIRTHDATE (DD.MM.YYYY): ");
                                bdate = Console.ReadLine();
                                char[] bdateCharArray = bdate.ToCharArray();
                                bool crit = true;
                                if (bdate != "")
                                {
                                    for (int i = 0; i < bdate.Split('.').Length; i++)
                                    {
                                        crit &= bdate.Split('.')[i].All(char.IsNumber);
                                    }
                                    crit &= bdateCharArray[2] == '.' && bdateCharArray[5] == '.' && bdateCharArray.Length == 10;
                                }
                                if (crit)
                                {
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("Please, be careful, BIRTHDATE must have the form DD.MM.YYYY!");
                                }
                            }
                            if (bdate != "")
                            {
                                d = int.Parse(bdate.Split('.')[0]);
                                m = int.Parse(bdate.Split('.')[1]);
                                y = int.Parse(bdate.Split('.')[2]);
                                kv.Value.birthDate = new DateTime(y, m, d);
                                Console.WriteLine("BIRTHDATE has been updated!");
                            }
                        }
                        else if (p == 7)
                        {
                            Console.WriteLine("OLD ORGANIZATION: " + kv.Value.organization);
                            Console.Write("NEW ORGANIZATION: ");
                            kv.Value.organization = Console.ReadLine();
                            Console.WriteLine("ORGANIZATION has been updated!");
                        }
                        else if (p == 8)
                        {
                            Console.WriteLine("OLD POSITION: " + kv.Value.position);
                            Console.Write("NEW POSITION: ");
                            kv.Value.position = Console.ReadLine();
                            Console.WriteLine("POSITION has been updated!");
                        }
                        else if (p == 9)
                        {
                            Console.WriteLine("OLD COMMENTS: " + kv.Value.comments);
                            Console.Write("NEW COMMENTS: ");
                            kv.Value.comments = Console.ReadLine();
                            Console.WriteLine("COMMENTS have been updated!");
                        }
                        else
                        {
                            Console.WriteLine("I did not catch this, please, try again");
                        }
                    }
                    nonex = false;
                    break;
                }
            }
            if (nonex)
            {
                Console.WriteLine("The note with ID " + idUpd + " is not present in l'annuaire telephonique");
                Console.WriteLine("Please, try again");
            }
        }
        public static void RemoveNote(int idRem)
        {
            bool nonex = true;
            foreach (KeyValuePair<int, Note> kv in allNotes)
            {
                if (kv.Key == idRem)
                {
                    allNotes.Remove(idRem);
                    nonex = false;
                    Console.WriteLine("Note with ID " + idRem + " has been removed!");
                    break;
                }
            }
            if (nonex)
            {
                Console.WriteLine("Nothing to remove here, since the note with ID " + idRem + " is not present in l'annuaire telephonique");
                Console.WriteLine("Please, try again");
            }
            
        }
        public static void AppealToNote(int idFind)
        {
            bool nonex = true;
            Console.WriteLine("---------------------------");
            foreach (KeyValuePair<int, Note> kv in allNotes)
            {
                if (kv.Key == idFind)
                {
                    nonex = false;
                    Console.WriteLine("ID: " + kv.Key);
                    Console.WriteLine("SURNAME: " + kv.Value.surname);
                    Console.WriteLine("NAME: " + kv.Value.name);
                    Console.WriteLine("PATRONYMIC: " + kv.Value.patronymic);
                    Console.WriteLine("PHONE: " + kv.Value.phone);
                    Console.WriteLine("COUNTRY: " + kv.Value.country);
                    if (kv.Value.birthDate.ToShortDateString() != "01.01.0001")
                    {
                        Console.WriteLine("BIRTHDATE: " + kv.Value.birthDate.ToShortDateString());
                    }
                    else
                    {
                        Console.WriteLine("BIRTHDATE: -");
                    }
                    Console.WriteLine("ORGANIZATION: "+ kv.Value.organization);
                    Console.WriteLine("POSITION: " + kv.Value.position);
                    Console.WriteLine("COMMENTS: " + kv.Value.comments);
                    Console.WriteLine("---------------------------");
                }
            }
            if (nonex)
            {
                Console.WriteLine("The ID " + idFind + " is not present in l'annuaire telephonique. Please, try again");
            }
        }
        public static void ListAllNotes()
        {
            Console.WriteLine("You have selected to list all the notes");
            if (allNotes.Count != 0)
            {
                Console.WriteLine("L'annuaire telephonique list:");
                Console.WriteLine("---------------------------");
                foreach (KeyValuePair<int, Note> kv in allNotes)
                {
                    Console.WriteLine("ID: " + kv.Key);
                    Console.WriteLine("SURNAME: " + kv.Value.surname);
                    Console.WriteLine("NAME: " + kv.Value.name);
                    Console.WriteLine("PHONE: " + kv.Value.phone);
                    Console.WriteLine("---------------------------");
                }
            }
            else
            {
                Console.WriteLine("... but there is nothing to show yet!");
            }
            
        }
    }
}
