﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotePad
{
    class Program
    {
        static void Main(string[] args)
        {
            int cnt = 0;
            Console.WriteLine("Bonjour! Bienvenue dans l'annuaire telephonique!");
            while (true)
            {
                Console.WriteLine("To create a note, enter 1");
                Console.WriteLine("To update a note, enter 2");
                Console.WriteLine("To remove a note, enter 3");
                Console.WriteLine("To see a single note, enter 4");
                Console.WriteLine("To see the note list, enter 5");
                Console.WriteLine("If you want to end your session, enter 0");
                string pp = "";
                while (true)
                {
                    pp = Console.ReadLine();
                    if (pp.All(char.IsDigit) && pp != "")
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please, be careful and try again, you should enter digits ONLY!");
                    }
                }
                int p = int.Parse(pp);
                if (p == 0)
                {
                    break;
                }
                else if (p == 1)
                {
                    cnt++;
                    Note.NewNote(cnt);
                }
                else if (p == 2)
                {
                    Console.WriteLine("You have selected to update a note. Please, provide an ID of the note you want to update");
                    Console.Write("ID: ");
                    int idUpd = int.Parse(Console.ReadLine());
                    Note.UpdateNote(idUpd);
                }
                else if (p == 3)
                {
                    Console.WriteLine("You have selected to remove a note. Please, provide an ID of the note you want to remove");
                    Console.Write("ID: ");
                    int idRem = int.Parse(Console.ReadLine());
                    Note.RemoveNote(idRem);
                }
                else if (p == 4)
                {
                    Console.WriteLine("You have selected to find a note. Please, provide an ID of the note you want to find");
                    Console.Write("ID: ");
                    int idFind = int.Parse(Console.ReadLine());
                    Note.AppealToNote(idFind);
                }
                else if (p == 5)
                {
                    Note.ListAllNotes();
                }
                else
                {
                    Console.WriteLine("I did not catch this, please, try again");
                }
            }
        }
    }
}
